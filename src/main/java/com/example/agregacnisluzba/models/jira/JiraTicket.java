package com.example.agregacnisluzba.models.jira;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JiraTicket {


    private int id;
    private String name;
    private String email;
    private int idPersonCreator;
    private int idPersonAssigned;
    private String creationDatetime;
    private String ticketCloseDatetime;

    public JiraTicket() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(int idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    public int getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(int idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public String getCreationDatetime() {
        return creationDatetime;
    }

    public void setCreationDatetime(String creationDatetime) {
        this.creationDatetime = creationDatetime;
    }

    public String getTicketCloseDatetime() {
        return ticketCloseDatetime;
    }

    public void setTicketCloseDatetime(String ticketCloseDatetime) {
        this.ticketCloseDatetime = ticketCloseDatetime;
    }
}
