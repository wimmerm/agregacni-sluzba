package com.example.agregacnisluzba.models.serviceNow;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceNowTicket {


    private int id;
    private String name;
    private String email;
    private int idPersonCreator;
    private int idPersonAssigned;
    private String creationDateTime;

    public ServiceNowTicket() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(int idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    public int getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(int idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public String getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(String creationDateTime) {
        this.creationDateTime = creationDateTime;
    }
}
