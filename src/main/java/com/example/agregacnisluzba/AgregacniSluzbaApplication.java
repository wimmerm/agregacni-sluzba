package com.example.agregacnisluzba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class AgregacniSluzbaApplication {

    public static void main(String[] args) {
        SpringApplication.run(AgregacniSluzbaApplication.class, args);
    }

    @Bean
    public RestTemplate getTemplate(){
        return new RestTemplate();
    }

}
