package com.example.agregacnisluzba.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SwaggerConfig {

    @Bean
    public Docket ticketApi(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.agregacnisluzba"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(ticketInfo());
    }

    private ApiInfo ticketInfo(){
        ApiInfo apiInfo = new ApiInfo(
                "Seminarka Service Now Swagger Dokumentace",
                "Swagger dokumentace dle zadani (snad...)",
                "1.0",
                "Terms of Service = null",
                new Contact("Marek Wimmer","test","marek.wimmer@gmail.com"),"test","www.test.test");
        return apiInfo;

    }

}