package com.example.agregacnisluzba.controllers.serviceNowController;

import com.example.agregacnisluzba.models.serviceNow.ServiceNowTicket;
import com.example.agregacnisluzba.service.ServiceNowService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("integrator/user")
@Api(value = "Service Now Rest Endpoint")
public class ServiceNowController {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ServiceNowService serviceNowService;


    private final String baseUrl = "http://localhost:8080//tickets";

    @ApiOperation(value = "Find Ticket by Id")
    @GetMapping(path = "/{id}", produces = "application/json")
    public ServiceNowTicket getTicketsById(@PathVariable("id") Integer id){
        ResponseEntity<ServiceNowTicket> response = restTemplate.getForEntity(baseUrl+"/find/"+id , ServiceNowTicket.class);
        return  response.getBody();

    }

    @ApiOperation(value = "Find Ticket by Name")
    @GetMapping(path = "/find/{name}", produces ="application/json")
    public ServiceNowTicket getTicketsByName(@PathVariable("name")String name){
        ResponseEntity<ServiceNowTicket> response = restTemplate.getForEntity(baseUrl+"/findBy/"+name , ServiceNowTicket.class);
        return response.getBody();
    }

    @ApiOperation(value = "add new Ticket")
    @PostMapping(path = "/add" , consumes = "application/json", produces ="application/json")
    public ServiceNowTicket addTicket(@RequestBody ServiceNowTicket serviceNowTicket){
        return serviceNowService.addTicket(serviceNowTicket);
    }

}
