package com.example.agregacnisluzba.controllers.userManagementController;

import com.example.agregacnisluzba.models.userManagement.Person;
import com.example.agregacnisluzba.service.ServiceNowService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("user")
@Api(value = "User Management Endpoint")
public class UserManagementController {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ServiceNowService serviceNowService;


    private final String baseUrl = "https://localhost:8443/user-management";

    @ApiOperation(value = "Find Person by Id")
    @GetMapping(path = "/{id}", produces = "application/json")
    public Person getPersonById(@PathVariable("id") Integer id){
        ResponseEntity<Person> response = restTemplate.getForEntity(baseUrl+"/kotlar/find/"+id , Person.class);
        return  response.getBody();

    }

    @ApiOperation(value = "Find Person by email")
    @GetMapping(path = "/find/{email}", produces ="application/json")
    public Person getTicketsByEmail(@PathVariable("email")String email){
        ResponseEntity<Person> response = restTemplate.getForEntity(baseUrl+"/kotlar/manager/{"+email , Person.class);
        return response.getBody();
    }
}
