package com.example.agregacnisluzba.controllers.jiraController;

import com.example.agregacnisluzba.generated.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

@Service
public class JiraClient {

    @Autowired
    private Jaxb2Marshaller marshaller;

    private WebServiceTemplate template;

    private final String URI = "http://localhost:8080/ws";

    public GetTicketIdResponse getTicketById(GetTicketIdRequest request){
        template = new WebServiceTemplate(marshaller);
        GetTicketIdResponse response = (GetTicketIdResponse) template.marshalSendAndReceive(URI,request);
        return response;
    }

    public GetTicketResponse geticketByName(GetTicketRequest request){
        template = new WebServiceTemplate(marshaller);
        GetTicketResponse response = (GetTicketResponse) template.marshalSendAndReceive(URI,request);
        return response;
    }

    public AddTicketResponse addTicket(AddTicketRequest request){
        template = new WebServiceTemplate(marshaller);
        AddTicketResponse response = (AddTicketResponse) template.marshalSendAndReceive(URI,request);
        return response;
    }
    
}
