package com.example.agregacnisluzba.controllers.jiraController;

import com.example.agregacnisluzba.generated.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value = "Jira Endpoint")
public class JiraController {

    @Autowired
    private JiraClient client;

    @ApiOperation(value = "Find Ticket by Id")
    @PostMapping("/get/id")
    public GetTicketIdResponse getTicketById(@RequestBody GetTicketIdRequest request) {
        return client.getTicketById(request);
    }

    @ApiOperation(value = "Find Ticket by Name")
    @PostMapping("/get/name")
    public GetTicketResponse getTicketByName(@RequestBody GetTicketRequest request) {
        return client.geticketByName(request);
    }

    @ApiOperation(value = "Adds ticket")
    @PostMapping("/add")
    public AddTicketResponse addTicket(@RequestBody AddTicketRequest request) {
        return client.addTicket(request);
    }
}
